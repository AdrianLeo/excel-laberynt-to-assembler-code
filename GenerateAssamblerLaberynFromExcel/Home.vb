﻿Imports System.ComponentModel
Imports ClosedXML.Excel

Public Class Home

    Private WB As IXLWorkbook
    Private WS As IXLWorksheet

    Private selectedColor As BIOSColors
    Private currentColor As Color
    Private count As Integer

    Private Enum BIOSColors
        Black = 0
        Blue = 1
        Green = 2
        Cyan = 3
        Red = 4
        Magenta = 5
        Brown = 6
        LightGray = 7
        DarkGray = 8
        LightBlue = 9
        LightGreen = 10
        LightCyan = 11
        LightRed = 12
        LightMagenta = 13
        Yellow = 14
        White = 15
    End Enum

    Private Sub BTExaminar_Click(sender As Object, e As EventArgs) Handles BTExaminar.Click
        Dim Resultado As DialogResult = OFDExaminar.ShowDialog

        If Resultado = DialogResult.OK Then
            TBRuta.Text = OFDExaminar.FileName
        End If
    End Sub

    Private Sub BTProcesar_Click(sender As Object, e As EventArgs) Handles BTProcesar.Click
        WB = New XLWorkbook(TBRuta.Text)
        WS = WB.Worksheets.First()
        selectedColor = CBColorDefault.SelectedIndex
        BTProcesar.Enabled = False
        TBResultado.Text = ""
        BGWProcesar.RunWorkerAsync()
    End Sub

    Private Sub BGWProcesar_DoWork(sender As Object, e As DoWorkEventArgs) Handles BGWProcesar.DoWork
        For Each rowIndex In Enumerable.Range(1, 24)
            Dim row = WS.Row(rowIndex)
            Dim cell = row.FirstCell

            Dim lastColumn As Integer
            Dim lastCount As Integer

            count = 0
            currentColor = Nothing

            For Each columnIndex In Enumerable.Range(1, 80)
                Dim color = cell.Style.Fill.BackgroundColor.Color

                If currentColor = Nothing Then
                    currentColor = color
                    count = 1
                ElseIf currentColor = color Then
                    count += 1
                Else
                    lastColumn = columnIndex - count - 1
                    lastCount = count

                    BGWProcesar.ReportProgress(rowIndex * 80 + columnIndex, $"Dibujar {rowIndex - 1} {lastColumn} {lastCount} {If(currentColor = Color.Transparent, 0, Convert.ToInt32(selectedColor))} {vbNewLine}")

                    currentColor = color
                    count = 1
                End If

                cell = cell.CellRight
            Next

            BGWProcesar.ReportProgress(80 * 25, $"Dibujar {rowIndex - 1} {lastColumn + lastCount} {80 - (lastColumn + lastCount)} {Convert.ToInt32(selectedColor)} {vbNewLine}")

            lastColumn = 0
            lastCount = 0
        Next
    End Sub

    Private Sub BGWProcesar_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles BGWProcesar.RunWorkerCompleted
        MessageBox.Show("La generación del código de ensamblador ha terminado.")
        BTProcesar.Enabled = True
        WB.Dispose()
    End Sub

    Private Sub BGWProcesar_ProgressChanged(sender As Object, e As ProgressChangedEventArgs) Handles BGWProcesar.ProgressChanged
        If e.UserState IsNot Nothing Then
            TBResultado.AppendText(e.UserState)
        End If
        TSPBProgreso.Value = e.ProgressPercentage
    End Sub

    Private Sub BTCancelar_Click(sender As Object, e As EventArgs) Handles BTCancelar.Click
        BGWProcesar.CancelAsync()
    End Sub

    Private Sub Inicio_Load(sender As Object, e As EventArgs) Handles Me.Load
        CBColorDefault.DataSource = [Enum].GetNames(GetType(BIOSColors))

        TSPBProgreso.Maximum = 80 * 25
    End Sub

    Private Sub TBResultado_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles TBResultado.MouseDoubleClick
        TBResultado.SelectAll()
        TBResultado.Copy()
        TSSLEstado.Text = "Code copied."
    End Sub
End Class
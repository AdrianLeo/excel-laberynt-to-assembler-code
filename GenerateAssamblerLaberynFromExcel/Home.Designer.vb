﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Home
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim Label1 As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Me.OFDExaminar = New System.Windows.Forms.OpenFileDialog()
        Me.TBResultado = New System.Windows.Forms.TextBox()
        Me.TBRuta = New System.Windows.Forms.TextBox()
        Me.BTExaminar = New System.Windows.Forms.Button()
        Me.BTProcesar = New System.Windows.Forms.Button()
        Me.BGWProcesar = New System.ComponentModel.BackgroundWorker()
        Me.BTCancelar = New System.Windows.Forms.Button()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.TSPBProgreso = New System.Windows.Forms.ToolStripProgressBar()
        Me.TSSLEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.CBColorDefault = New System.Windows.Forms.ComboBox()
        Label1 = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Location = New System.Drawing.Point(12, 9)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(85, 13)
        Label1.TabIndex = 4
        Label1.Text = "Ruta del archivo"
        '
        'OFDExaminar
        '
        Me.OFDExaminar.FileName = "OFDLeer"
        '
        'TBResultado
        '
        Me.TBResultado.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBResultado.Location = New System.Drawing.Point(12, 91)
        Me.TBResultado.Multiline = True
        Me.TBResultado.Name = "TBResultado"
        Me.TBResultado.ReadOnly = True
        Me.TBResultado.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TBResultado.Size = New System.Drawing.Size(618, 230)
        Me.TBResultado.TabIndex = 0
        '
        'TBRuta
        '
        Me.TBRuta.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBRuta.Location = New System.Drawing.Point(12, 25)
        Me.TBRuta.Name = "TBRuta"
        Me.TBRuta.ReadOnly = True
        Me.TBRuta.Size = New System.Drawing.Size(537, 20)
        Me.TBRuta.TabIndex = 1
        '
        'BTExaminar
        '
        Me.BTExaminar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BTExaminar.Location = New System.Drawing.Point(555, 23)
        Me.BTExaminar.Name = "BTExaminar"
        Me.BTExaminar.Size = New System.Drawing.Size(75, 23)
        Me.BTExaminar.TabIndex = 2
        Me.BTExaminar.Text = "Examinar"
        Me.BTExaminar.UseVisualStyleBackColor = True
        '
        'BTProcesar
        '
        Me.BTProcesar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BTProcesar.Location = New System.Drawing.Point(555, 62)
        Me.BTProcesar.Name = "BTProcesar"
        Me.BTProcesar.Size = New System.Drawing.Size(75, 23)
        Me.BTProcesar.TabIndex = 3
        Me.BTProcesar.Text = "Procesar"
        Me.BTProcesar.UseVisualStyleBackColor = True
        '
        'BGWProcesar
        '
        Me.BGWProcesar.WorkerReportsProgress = True
        Me.BGWProcesar.WorkerSupportsCancellation = True
        '
        'BTCancelar
        '
        Me.BTCancelar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BTCancelar.Location = New System.Drawing.Point(474, 62)
        Me.BTCancelar.Name = "BTCancelar"
        Me.BTCancelar.Size = New System.Drawing.Size(75, 23)
        Me.BTCancelar.TabIndex = 5
        Me.BTCancelar.Text = "Cancelar"
        Me.BTCancelar.UseVisualStyleBackColor = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TSPBProgreso, Me.TSSLEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 324)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(642, 22)
        Me.StatusStrip1.TabIndex = 6
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'TSPBProgreso
        '
        Me.TSPBProgreso.Name = "TSPBProgreso"
        Me.TSPBProgreso.Size = New System.Drawing.Size(150, 16)
        '
        'TSSLEstado
        '
        Me.TSSLEstado.Name = "TSSLEstado"
        Me.TSSLEstado.Size = New System.Drawing.Size(0, 17)
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Location = New System.Drawing.Point(12, 48)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(88, 13)
        Label2.TabIndex = 7
        Label2.Text = "Color por defecto"
        '
        'CBColorDefault
        '
        Me.CBColorDefault.FormattingEnabled = True
        Me.CBColorDefault.Location = New System.Drawing.Point(12, 64)
        Me.CBColorDefault.Name = "CBColorDefault"
        Me.CBColorDefault.Size = New System.Drawing.Size(195, 21)
        Me.CBColorDefault.TabIndex = 8
        '
        'Inicio
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(642, 346)
        Me.Controls.Add(Me.CBColorDefault)
        Me.Controls.Add(Label2)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.BTCancelar)
        Me.Controls.Add(Label1)
        Me.Controls.Add(Me.BTProcesar)
        Me.Controls.Add(Me.BTExaminar)
        Me.Controls.Add(Me.TBRuta)
        Me.Controls.Add(Me.TBResultado)
        Me.Name = "Inicio"
        Me.Text = "Form1"
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents OFDExaminar As OpenFileDialog
    Friend WithEvents TBResultado As TextBox
    Friend WithEvents TBRuta As TextBox
    Friend WithEvents BTExaminar As Button
    Friend WithEvents BTProcesar As Button
    Friend WithEvents BGWProcesar As System.ComponentModel.BackgroundWorker
    Friend WithEvents BTCancelar As Button
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents TSPBProgreso As ToolStripProgressBar
    Friend WithEvents TSSLEstado As ToolStripStatusLabel
    Friend WithEvents CBColorDefault As ComboBox
End Class
